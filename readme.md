# MealPlanner2Go
MealPlanner2Go ist eine Schnittstelle zur Erstellung von Einkaufslisten innerhalb einer Gruppe oder für sich.
Hierzu werden vom Nutzenden Einkaufstage und Mahlzeiten festgelegt und die Applikation erstellt zugeschnittene Einkaufslisten für die jeweiligen Tage.

## Starten (mit Docker Compose)
### Services bauen und starten
Mit diesen Befehlen werden die Services der Applikation lokal gestartet.
```bash
docker compose build && docker compose up
```
### Services stoppen und entfernen
Mit diesem Befehl wird der Service entfernt. Volumes bleiben bestehen.
```bash
docker compose down
```
### Ungenutzte Volumes löschen
Mit diesem Befehl werden alle ungenutzten Volumes entfernt.
```bash
docker volume prune
```

## Nutzung der API
### Nutzerfunktionen
Nutzer erstellen
```bash
curl -H "Content-Type: application/json" -d '{"mail":"max.mustermann@gmail.com","firstName":"Max","lastName":"Mustermann"}' localhost:8000/user

```
Nutzer abrufen
```bash
curl localhost:8000/user/1
```
Nutzerliste abrufen
```bash
curl localhost:8000/user
```
Nutzer aktualisieren
```bash
curl -X PUT -H "Content-Type: application/json"  -d '{"mail":"max.mustermann@gmail.com","firstName":"Max","lastName":"Mustermann"}' localhost:8000/user/1
```
Nutzer entfernen
```bash
curl -X DELETE localhost:8000/user/1
```
Einkaufstage hinzufügen
```bash
curl -H "Content-Type: application/json" -d '{"userId":1,"date":"2023-07-12"}' localhost:8000/user/date
```
Einkaufstage entfernen
```bash
curl -X DELETE localhost:8000/user/date/1
```
Gericht hinzufügen
```bash
curl -H "Content-Type: application/json" -d '{"userId":1,"mealId":52992,"date":"2023-07-12"}' localhost:8000/user/meal
```
Gericht entfernen
```bash
curl -X DELETE localhost:8000/user/meal/1
```
Gericht favorisieren
```bash
curl -H "Content-Type: application/json" -d '{}' localhost:8000/user/1/52992
```
Gericht entfavorisieren
```bash
curl -X DELETE localhost:8000/user/1/52992
```
Einkaufsliste abrufen
```bash
curl localhost:8000/user/list/1
```
### Gruppenfunktionen
Gruppe erstellen
```bash
curl -H "Content-Type: application/json" -d '{"name":"Enterprise Development"}' localhost:8000/group

```
Gruppe abrufen
```bash
curl localhost:8000/group/1
```
Gruppenliste abrufen
```bash
curl localhost:8000/group
```
Gruppe entfernen
```bash
curl -X DELETE localhost:8000/group/1
```
Gericht hinzufügen
```bash
curl -H "Content-Type: application/json" -d '{"groupId":1,"mealId":52992,"date":"2023-07-12"}' localhost:8000/group/meal
```
Gericht entfernen
```bash
curl -X DELETE localhost:8000/group/meal/1
```
Nutzer hinzufügen
```bash
curl -H "Content-Type: application/json" -d '{}' localhost:8000/group/1/1
```
Nutzer entfernen
```bash
curl -X DELETE localhost:8000/group/1/1
```
### Gerichtfunktionalitäten (interne Kommunikation über gRPC)
Zufälliges Gericht abrufen
```bash
curl localhost:8000/meal/random
```
Bestimmtes Gericht abrufen
```bash
curl localhost:8000/meal/53043
```
Nach Gerichten suchen
```bash
curl localhost:8000/meal/search/eggplant
```
Kategorien einsehen
```bash
curl localhost:8000/meal/category
```
Nach Kategorien filtern
```bash
curl localhost:8000/meal/category/beef
```
Orte einsehen
```bash
curl localhost:8000/meal/area
```
Nach Orten filtern
```bash
curl localhost:8000/meal/area/american
```
Zutaten einsehen
```bash
curl localhost:8000/meal/ingredient
```
Nach Zutaten filtern
```bash
curl localhost:8000/meal/ingredient/chicken
```