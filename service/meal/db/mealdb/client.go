package mealdb

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/meal/grpc/meal"
)

const baseUrl = "https://www.themealdb.com/api/json/v1/1/"

var client = &http.Client{
	Timeout: time.Minute,
}

func GetRandomMeal() (*meal.MealResponse, error) {
	path := "random.php"
	request, err := http.NewRequest(http.MethodGet, baseUrl+path, nil)
	if err != nil {
		return nil, err
	}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	var mealResponse *MealResponse
	err = json.NewDecoder(response.Body).Decode(&mealResponse)
	if err != nil {
		return nil, err
	}

	if len(mealResponse.Meals) < 1 {
		return nil, errors.New("no meals within the response")
	}
	currentMeal := mealResponse.Meals[0]

	return createMeal(currentMeal)
}

func GetMeal(id int) (*meal.MealResponse, error) {
	path := fmt.Sprintf("lookup.php?i=%d", id)
	request, err := http.NewRequest(http.MethodGet, baseUrl+path, nil)
	if err != nil {
		return nil, err
	}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}

	var mealResponse *MealResponse
	err = json.NewDecoder(response.Body).Decode(&mealResponse)
	if err != nil {
		return nil, err
	}

	if len(mealResponse.Meals) < 1 {
		return nil, errors.New("no meals within the response")
	}
	currentMeal := mealResponse.Meals[0]

	return createMeal(currentMeal)
}

func SearchMealByName(name string) (*meal.MealResponses, error) {
	path := fmt.Sprintf("search.php?s=%s", name)
	request, err := http.NewRequest(http.MethodGet, baseUrl+path, nil)
	if err != nil {
		return nil, err
	}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	var mealResponse *MealResponse
	err = json.NewDecoder(response.Body).Decode(&mealResponse)
	if err != nil {
		return nil, err
	}

	meals := &meal.MealResponses{}
	for _, v := range mealResponse.Meals {
		meal, err := createMeal(v)
		if err != nil {
			continue
		}
		meals.Meals = append(meals.Meals, meal)
	}

	return meals, nil
}

func SearchMealBy(value string, filter string) (*meal.MealOverviewResponses, error) {
	path := fmt.Sprintf("filter.php?%s=%s", filter, value)
	request, err := http.NewRequest(http.MethodGet, baseUrl+path, nil)
	if err != nil {
		return nil, err
	}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	var mealResponse *MealResponse
	err = json.NewDecoder(response.Body).Decode(&mealResponse)
	if err != nil {
		return nil, err
	}

	meals := &meal.MealOverviewResponses{}
	for _, v := range mealResponse.Meals {
		meal, err := createMealOverview(v)
		if err != nil {
			continue
		}
		meals.Meals = append(meals.Meals, meal)
	}

	return meals, nil
}

func GetFilter(filter string) (*meal.FilterOverviewResponse, error) {
	path := fmt.Sprintf("list.php?%s=list", filter)
	request, err := http.NewRequest(http.MethodGet, baseUrl+path, nil)
	if err != nil {
		return nil, err
	}
	response, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	var mealResponse *MealResponse
	err = json.NewDecoder(response.Body).Decode(&mealResponse)
	if err != nil {
		return nil, err
	}
	filterOverview := &meal.FilterOverviewResponse{}
	switch filter {
	case "c":
		for _, v := range mealResponse.Meals {
			if v.StrCategory != nil {
				filterOverview.ValidValues = append(filterOverview.ValidValues, *v.StrCategory)
			}
		}
	case "a":
		for _, v := range mealResponse.Meals {
			if v.StrArea != nil {
				filterOverview.ValidValues = append(filterOverview.ValidValues, *v.StrArea)
			}
		}
	case "i":
		for _, v := range mealResponse.Meals {
			if v.StrIngredient != nil {
				filterOverview.ValidValues = append(filterOverview.ValidValues, *v.StrIngredient)
			}
		}
	}

	return filterOverview, nil
}

func createMeal(currentMeal *Meal) (*meal.MealResponse, error) {
	result := &meal.MealResponse{}
	if currentMeal.IdMeal != nil {
		id, err := strconv.Atoi(*currentMeal.IdMeal)
		if err == nil {
			result.Id = int32(id)
		}
	}
	if currentMeal.StrMeal != nil {
		result.Name = *currentMeal.StrMeal
	}
	if currentMeal.StrCategory != nil {
		result.Category = *currentMeal.StrCategory
	}
	if currentMeal.StrInstructions != nil {
		result.Instructions = *currentMeal.StrInstructions
	}
	if currentMeal.StrMealThumb != nil {
		result.ImagePath = *currentMeal.StrMealThumb
	}
	ingredients := []*meal.IngredientResponse{}
	if currentMeal.StrIngredient1 != nil {
		if currentMeal.StrMeasure1 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient1,
				Amount: *currentMeal.StrMeasure1,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient1,
			})
		}
	}
	if currentMeal.StrIngredient2 != nil {
		if currentMeal.StrMeasure2 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient2,
				Amount: *currentMeal.StrMeasure2,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient2,
			})
		}
	}
	if currentMeal.StrIngredient3 != nil {
		if currentMeal.StrMeasure3 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient3,
				Amount: *currentMeal.StrMeasure3,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient3,
			})
		}
	}
	if currentMeal.StrIngredient4 != nil {
		if currentMeal.StrMeasure4 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient4,
				Amount: *currentMeal.StrMeasure4,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient4,
			})
		}
	}
	if currentMeal.StrIngredient5 != nil {
		if currentMeal.StrMeasure5 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient5,
				Amount: *currentMeal.StrMeasure5,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient5,
			})
		}
	}
	if currentMeal.StrIngredient6 != nil {
		if currentMeal.StrMeasure6 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient6,
				Amount: *currentMeal.StrMeasure6,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient6,
			})
		}
	}
	if currentMeal.StrIngredient7 != nil {
		if currentMeal.StrMeasure7 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient7,
				Amount: *currentMeal.StrMeasure7,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient7,
			})
		}
	}
	if currentMeal.StrIngredient8 != nil {
		if currentMeal.StrMeasure8 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient8,
				Amount: *currentMeal.StrMeasure8,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient8,
			})
		}
	}
	if currentMeal.StrIngredient9 != nil {
		if currentMeal.StrMeasure9 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient9,
				Amount: *currentMeal.StrMeasure9,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient9,
			})
		}
	}
	if currentMeal.StrIngredient10 != nil {
		if currentMeal.StrMeasure10 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient10,
				Amount: *currentMeal.StrMeasure10,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient10,
			})
		}
	}
	if currentMeal.StrIngredient11 != nil {
		if currentMeal.StrMeasure11 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient11,
				Amount: *currentMeal.StrMeasure11,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient11,
			})
		}
	}
	if currentMeal.StrIngredient12 != nil {
		if currentMeal.StrMeasure12 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient12,
				Amount: *currentMeal.StrMeasure12,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient12,
			})
		}
	}
	if currentMeal.StrIngredient13 != nil {
		if currentMeal.StrMeasure13 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient13,
				Amount: *currentMeal.StrMeasure13,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient13,
			})
		}
	}
	if currentMeal.StrIngredient14 != nil {
		if currentMeal.StrMeasure14 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient14,
				Amount: *currentMeal.StrMeasure14,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient14,
			})
		}
	}
	if currentMeal.StrIngredient15 != nil {
		if currentMeal.StrMeasure15 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient15,
				Amount: *currentMeal.StrMeasure15,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient15,
			})
		}
	}
	if currentMeal.StrIngredient16 != nil {
		if currentMeal.StrMeasure16 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient16,
				Amount: *currentMeal.StrMeasure16,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient16,
			})
		}
	}
	if currentMeal.StrIngredient17 != nil {
		if currentMeal.StrMeasure17 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient17,
				Amount: *currentMeal.StrMeasure17,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient17,
			})
		}
	}
	if currentMeal.StrIngredient18 != nil {
		if currentMeal.StrMeasure18 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient18,
				Amount: *currentMeal.StrMeasure18,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient18,
			})
		}
	}
	if currentMeal.StrIngredient19 != nil {
		if currentMeal.StrMeasure19 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient19,
				Amount: *currentMeal.StrMeasure19,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient19,
			})
		}
	}
	if currentMeal.StrIngredient20 != nil {
		if currentMeal.StrMeasure20 != nil {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name:   *currentMeal.StrIngredient20,
				Amount: *currentMeal.StrMeasure20,
			})
		} else {
			ingredients = append(ingredients, &meal.IngredientResponse{
				Name: *currentMeal.StrIngredient20,
			})
		}
	}
	for _, ingredient := range ingredients {
		if ingredient.Name != "" {
			result.Ingredients = append(result.Ingredients, ingredient)
		}
	}

	return result, nil
}

func createMealOverview(currentMeal *Meal) (*meal.MealOverviewResponse, error) {
	meal := &meal.MealOverviewResponse{}
	if currentMeal.IdMeal != nil {
		id, err := strconv.Atoi(*currentMeal.IdMeal)
		if err == nil {
			meal.Id = int32(id)
		}
	}
	if currentMeal.StrMeal != nil {
		meal.Name = *currentMeal.StrMeal
	}
	if currentMeal.StrMealThumb != nil {
		meal.ImagePath = *currentMeal.StrMealThumb
	}

	return meal, nil
}
