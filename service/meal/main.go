package main

import (
	"fmt"

	"net"
	"os"

	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/meal/grpc/meal"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/meal/service"
	"google.golang.org/grpc"
)

func init() {
	// init logger
	log.SetFormatter(&log.TextFormatter{})
	log.SetReportCaller(true)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		log.Info("Log level not specified, set default to: INFO")
		log.SetLevel(log.InfoLevel)
		return
	}
	log.SetLevel(level)
}

func main() {
	port := 9111
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		log.Fatalf("failed to listen on grpc port %d: %v", port, err)
	}
	log.Infof("Server is listening on port :%v\n", port)
	grpcServer := grpc.NewServer()
	meal.RegisterMealServer(grpcServer, service.NewMealService())
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}
