package service

import (
	"context"

	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/meal/db/mealdb"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/meal/grpc/meal"
	"google.golang.org/protobuf/types/known/emptypb"
)

type MealService struct {
	meal.MealServer
}

func NewMealService() *MealService {
	return &MealService{}
}

func (ms *MealService) GetRandomMeal(_ context.Context, _ *emptypb.Empty) (*meal.MealResponse, error) {
	result, err := mealdb.GetRandomMeal()
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (ms *MealService) GetMeal(_ context.Context, mealRequest *meal.MealRequest) (*meal.MealResponse, error) {
	result, err := mealdb.GetMeal(int(mealRequest.Id))
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (ms *MealService) SearchMealByName(_ context.Context, searchRequest *meal.SearchRequest) (*meal.MealResponses, error) {
	result, err := mealdb.SearchMealByName(searchRequest.Name)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (ms *MealService) SearchMealBy(_ context.Context, mealOverviewRequest *meal.MealOverviewRequest) (*meal.MealOverviewResponses, error) {
	result, err := mealdb.SearchMealBy(mealOverviewRequest.Value, mealOverviewRequest.Filter)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func (ms *MealService) GetFilter(_ context.Context, filterOverviewRequest *meal.FilterOverviewRequest) (*meal.FilterOverviewResponse, error) {
	result, err := mealdb.GetFilter(filterOverviewRequest.Filter)
	if err != nil {
		return nil, err
	}

	return result, nil
}
