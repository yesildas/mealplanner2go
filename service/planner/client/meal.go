package client

import (
	"context"
	"os"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
)

var (
	mealTarget = os.Getenv("MEAL_CONNECT")
)

func GetMealConnection(ctx context.Context) (*grpc.ClientConn, error) {
	var err error
	log.Infof("Connecting to meal service via %s\n", mealTarget)
	conn, err := grpc.DialContext(ctx, mealTarget, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	return conn, nil
}
