package db

import (
	"database/sql"
	"errors"
	"os"
	"time"

	"github.com/go-sql-driver/mysql"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
)

const driverName = "mysql"

var cfg *mysql.Config

func Init() error {
	vp := viper.New()
	vp.AddConfigPath("./db")
	vp.SetConfigName("config")
	vp.SetConfigType("json")

	err := vp.ReadInConfig()
	if err != nil {
		return err
	}

	cfg = &mysql.Config{
		User:                 vp.GetString("user"),
		Passwd:               vp.GetString("passwd"),
		Net:                  "tcp",
		Addr:                 os.Getenv("DB_CONNECT"),
		DBName:               vp.GetString("dbName"),
		Loc:                  time.Now().Local().Location(),
		MaxAllowedPacket:     64 * 1024 * 1024,
		AllowNativePasswords: true,
		CheckConnLiveness:    true,
		ParseTime:            true,
	}

	log.Info("Using DSN for DB: ", cfg.FormatDSN())
	return nil
}

func CreateUser(mail string, firstName string, lastName string) (*model.User, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	queryCheck, err := udb.Query("SELECT * FROM userAccount WHERE mail = ?;", mail)
	if err != nil {
		return nil, err
	}
	defer queryCheck.Close()
	if queryCheck.Next() {
		return nil, errors.New("account exists")
	}

	query, err := udb.Query("INSERT INTO userAccount (mail, firstName, lastName) VALUES (?, ?, ?);", mail, firstName, lastName)
	if err != nil {
		return nil, err
	}
	query.Close()

	queryGetId, err := udb.Query("SELECT id FROM userAccount WHERE mail = ?;", mail)
	if err != nil {
		return nil, err
	}
	defer queryGetId.Close()
	var id int
	for queryGetId.Next() {
		err = queryGetId.Scan(&id)
		if err != nil {
			return nil, err
		}
	}

	return GetUser(id)
}

func GetUser(id int) (*model.User, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	query, err := udb.Query("SELECT * FROM userAccount WHERE id = ?;", id)
	if err != nil {
		return nil, err
	}
	defer query.Close()
	user := &model.User{}
	for query.Next() {
		err = query.Scan(&user.ID, &user.Mail, &user.FirstName, &user.LastName)
		if err != nil {
			return nil, err
		}
	}

	if user.Mail == "" {
		return nil, errors.New("account not found")
	}

	queryGroupIds, err := udb.Query("SELECT groupId FROM userGroupRelation WHERE userId = ?;", id)
	if err != nil {
		return nil, err
	}
	defer queryGroupIds.Close()
	for queryGroupIds.Next() {
		var groupId int
		err = queryGroupIds.Scan(&groupId)
		if err != nil {
			continue
		}
		user.GroupIds = append(user.GroupIds, groupId)
	}

	queryMeals, err := udb.Query("SELECT * FROM userMeal WHERE userId = ?;", id)
	if err != nil {
		return nil, err
	}
	defer queryMeals.Close()
	for queryMeals.Next() {
		var meal model.UserMealWithId
		err = queryMeals.Scan(&meal.ID, &meal.UserID, &meal.MealID, &meal.Date)
		if err != nil {
			continue
		}
		user.Meals = append(user.Meals, meal)
	}

	queryShoppingDates, err := udb.Query("SELECT * FROM shoppingDate WHERE userId = ?;", id)
	if err != nil {
		return nil, err
	}
	defer queryShoppingDates.Close()
	for queryShoppingDates.Next() {
		var date model.ShoppingDateWithId
		err = queryShoppingDates.Scan(&date.ID, &date.UserID, &date.Date)
		if err != nil {
			continue
		}
		user.ShoppingDates = append(user.ShoppingDates, date)
	}

	queryFavoriteMeals, err := udb.Query("SELECT mealId FROM favoriteMeal WHERE userId = ?;", id)
	if err != nil {
		return nil, err
	}
	defer queryFavoriteMeals.Close()
	for queryFavoriteMeals.Next() {
		var mealId int
		err = queryFavoriteMeals.Scan(&mealId)
		if err != nil {
			continue
		}
		user.FavoriteMeal = append(user.FavoriteMeal, mealId)
	}

	return user, nil
}

func GetUsers() ([]*model.UserItem, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	query, err := udb.Query("SELECT * FROM userAccount;")
	if err != nil {
		return nil, err
	}
	defer query.Close()
	var users []*model.UserItem
	for query.Next() {
		user := &model.UserItem{}
		err = query.Scan(&user.ID, &user.Mail, &user.FirstName, &user.LastName)
		if err != nil {
			continue
		}
		users = append(users, user)
	}

	if len(users) <= 0 {
		return nil, errors.New("no accounts found")
	}

	return users, nil
}

func UpdateUser(id int, mail string, firstName string, lastName string) (*model.User, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	query, err := udb.Query("UPDATE userAccount SET mail = ?, firstName = ?, lastName = ? WHERE id = ?;", mail, firstName, lastName, id)
	if err != nil {
		return nil, err
	}
	query.Close()

	return GetUser(id)
}

func DeleteUser(id int) (*model.User, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	user, err := GetUser(id)
	if err != nil {
		return nil, err
	}

	queryMeals, err := udb.Query("DELETE FROM userMeal WHERE userId = ?;", id)
	if err != nil {
		return nil, err
	}
	queryMeals.Close()

	queryShoppingDates, err := udb.Query("DELETE FROM shoppingDate WHERE userId = ?;", id)
	if err != nil {
		return nil, err
	}
	queryShoppingDates.Close()

	queryRelation, err := udb.Query("DELETE FROM userGroupRelation WHERE userId = ?;", id)
	if err != nil {
		return nil, err
	}
	queryRelation.Close()

	query, err := udb.Query("DELETE FROM userAccount WHERE id = ?;", id)
	if err != nil {
		return nil, err
	}
	query.Close()

	return user, nil
}

func CreateGroup(name string) (*model.Group, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	queryCheck, err := udb.Query("SELECT * FROM userGroup WHERE name = ?;", name)
	if err != nil {
		return nil, err
	}
	defer queryCheck.Close()
	if queryCheck.Next() {
		return nil, errors.New("group name exists")
	}

	query, err := udb.Query("INSERT INTO userGroup (name) VALUES (?);", name)
	if err != nil {
		return nil, err
	}
	query.Close()

	queryGetId, err := udb.Query("SELECT id FROM userGroup WHERE name = ?;", name)
	if err != nil {
		return nil, err
	}
	defer queryGetId.Close()
	var id int
	for queryGetId.Next() {
		err = queryGetId.Scan(&id)
		if err != nil {
			return nil, err
		}
	}

	return GetGroup(id)
}

func GetGroup(id int) (*model.Group, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	query, err := udb.Query("SELECT * FROM userGroup WHERE id = ?;", id)
	if err != nil {
		return nil, err
	}
	defer query.Close()
	group := &model.Group{}
	for query.Next() {
		err = query.Scan(&group.ID, &group.Name)
		if err != nil {
			return nil, err
		}
	}

	if group.Name == "" {
		return nil, errors.New("group not found")
	}

	queryMeals, err := udb.Query("SELECT * FROM groupMeal WHERE groupId = ?;", id)
	if err != nil {
		return nil, err
	}
	defer queryMeals.Close()
	for queryMeals.Next() {
		var meal model.GroupMealWithId
		err = queryMeals.Scan(&meal.ID, &meal.GroupID, &meal.MealID, &meal.Date)
		if err != nil {
			continue
		}
		group.Meals = append(group.Meals, meal)
	}

	queryUserIds, err := udb.Query("SELECT userId FROM userGroupRelation WHERE groupId = ?;", id)
	if err != nil {
		return nil, err
	}
	defer queryUserIds.Close()
	for queryUserIds.Next() {
		var userId int
		err = queryUserIds.Scan(&userId)
		if err != nil {
			continue
		}
		group.UserIDs = append(group.UserIDs, userId)
	}

	return group, nil
}

func GetGroups() ([]*model.GroupItem, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	query, err := udb.Query("SELECT * FROM userGroup;")
	if err != nil {
		return nil, err
	}
	defer query.Close()
	var groups []*model.GroupItem
	for query.Next() {
		group := &model.GroupItem{}
		err = query.Scan(&group.ID, &group.Name)
		if err != nil {
			continue
		}
		groups = append(groups, group)
	}

	if len(groups) <= 0 {
		return nil, errors.New("no groups found")
	}

	return groups, nil
}

func DeleteGroup(id int) (*model.Group, error) {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return nil, err
	}
	defer udb.Close()

	group, err := GetGroup(id)
	if err != nil {
		return nil, err
	}

	queryMeals, err := udb.Query("DELETE FROM groupMeal WHERE groupId = ?;", id)
	if err != nil {
		return nil, err
	}
	queryMeals.Close()

	queryRelation, err := udb.Query("DELETE FROM userGroupRelation WHERE groupId = ?;", id)
	if err != nil {
		return nil, err
	}
	queryRelation.Close()

	query, err := udb.Query("DELETE FROM userGroup WHERE id = ?;", id)
	if err != nil {
		return nil, err
	}
	query.Close()

	return group, nil
}

func CreateUserMeal(userId int, mealId int, date string) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	query, err := udb.Query("INSERT INTO userMeal (userId, mealId, date) VALUES (?, ?, ?);", userId, mealId, date)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func DeleteUserMeal(id int) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	query, err := udb.Query("DELETE FROM userMeal WHERE id = ?;", id)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func CreateFavoriteMeal(userId int, mealId int) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	queryCheck, err := udb.Query("SELECT * FROM favoriteMeal WHERE userId = ? AND mealId = ?;", userId, mealId)
	if err != nil {
		return err
	}
	defer queryCheck.Close()
	if queryCheck.Next() {
		return errors.New("favorite meal already added")
	}

	query, err := udb.Query("INSERT INTO favoriteMeal (userId, mealId) VALUES (?, ?);", userId, mealId)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func DeleteFavoriteMeal(userId int, mealId int) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	query, err := udb.Query("DELETE FROM favoriteMeal WHERE userId = ? AND mealId = ?;", userId, mealId)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func CreateGroupMeal(groupId int, mealId int, date string) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	query, err := udb.Query("INSERT INTO groupMeal (groupId, mealId, date) VALUES (?, ?, ?);", groupId, mealId, date)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func DeleteGroupMeal(id int) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	query, err := udb.Query("DELETE FROM groupMeal WHERE id = ?;", id)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func CreateShoppingDate(userId int, date string) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	query, err := udb.Query("INSERT INTO shoppingDate (userId, date) VALUES (?, ?);", userId, date)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func DeleteShoppingDate(id int) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	query, err := udb.Query("DELETE FROM shoppingDate WHERE id = ?;", id)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func CreateUserGroupRelation(userId int, groupId int) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	queryCheck, err := udb.Query("SELECT * FROM userGroupRelation WHERE userId = ? AND groupId = ?;", userId, groupId)
	if err != nil {
		return err
	}
	defer queryCheck.Close()
	if queryCheck.Next() {
		return errors.New("account already in group")
	}

	query, err := udb.Query("INSERT INTO userGroupRelation (userId, groupId) VALUES (?, ?);", userId, groupId)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}

func DeleteUserGroupRelation(userId int, groupId int) error {
	udb, err := sql.Open(driverName, cfg.FormatDSN())
	if err != nil {
		return err
	}
	defer udb.Close()

	query, err := udb.Query("DELETE FROM userGroupRelation WHERE userId = ? AND groupId = ?;", userId, groupId)
	if err != nil {
		return err
	}
	query.Close()

	return nil
}
