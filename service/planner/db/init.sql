CREATE DATABASE IF NOT EXISTS planner;

USE planner;

CREATE TABLE
    IF NOT EXISTS `userAccount` (
        `id` int NOT NULL AUTO_INCREMENT,
        `mail` varchar(255) NOT NULL,
        `firstName` varchar(255) NOT NULL,
        `lastName` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
    );

CREATE TABLE
    IF NOT EXISTS `userGroup` (
        `id` int NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        PRIMARY KEY (`id`)
    );

CREATE TABLE
    IF NOT EXISTS `userMeal` (
        `id` int NOT NULL AUTO_INCREMENT,
        `userId` int NOT NULL,
        `mealId` int NOT NULL,
        `date` DATE NOT NULL,
        PRIMARY KEY (`id`),
        FOREIGN KEY (`userId`) REFERENCES userAccount(`id`)
    );

CREATE TABLE
    IF NOT EXISTS `favoriteMeal` (
        `id` int NOT NULL AUTO_INCREMENT,
        `userId` int NOT NULL,
        `mealId` int NOT NULL,
        PRIMARY KEY (`id`),
        FOREIGN KEY (`userId`) REFERENCES userAccount(`id`)
    );

CREATE TABLE
    IF NOT EXISTS `groupMeal` (
        `id` int NOT NULL AUTO_INCREMENT,
        `groupId` int NOT NULL,
        `mealId` int NOT NULL,
        `date` DATE NOT NULL,
        PRIMARY KEY (`id`),
        FOREIGN KEY (`groupId`) REFERENCES userGroup(`id`)
    );

CREATE TABLE
    IF NOT EXISTS `shoppingDate` (
        `id` int NOT NULL AUTO_INCREMENT,
        `userId` int NOT NULL,
        `date` DATE NOT NULL,
        PRIMARY KEY (`id`),
        FOREIGN KEY (`userId`) REFERENCES userAccount(`id`)
    );

CREATE TABLE
    IF NOT EXISTS `userGroupRelation` (
        `id` int NOT NULL AUTO_INCREMENT,
        `groupId` int NOT NULL,
        `userId` int NOT NULL,
        PRIMARY KEY (`id`),
        FOREIGN KEY (`groupId`) REFERENCES userGroup(`id`),
        FOREIGN KEY (`userId`) REFERENCES userAccount(`id`)
    );