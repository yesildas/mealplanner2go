package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/service"
)

func getGroup(r *http.Request) (*model.Group, error) {
	var group model.Group
	err := json.NewDecoder(r.Body).Decode(&group)
	if err != nil {
		return nil, err
	}

	return &group, nil
}

func CreateGroup(w http.ResponseWriter, r *http.Request) {
	group, err := getGroup(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	group, err = service.CreateGroup(group)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, group)
}

func GetGroup(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	group, err := service.GetGroup(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, group)
}

func GetGroups(w http.ResponseWriter, r *http.Request) {
	groups, err := service.GetGroups()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, groups)
}

func DeleteGroup(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	group, err := service.DeleteGroup(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, group)
}

func CreateUserGroupRelation(w http.ResponseWriter, r *http.Request) {
	userId, err := getUserId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	groupId, err := getGroupId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := service.CreateUserGroupRelation(userId, groupId); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, nil)
}

func DeleteUserGroupRelation(w http.ResponseWriter, r *http.Request) {
	userId, err := getUserId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	groupId, err := getGroupId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = service.DeleteUserGroupRelation(userId, groupId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, nil)
}
