package handler

import (
	"net/http"

	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/service"
)

func GetRandomMeal(w http.ResponseWriter, r *http.Request) {
	meal, err := service.GetRandomMeal()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, meal)
}

func GetMeal(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	meal, err := service.GetMeal(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, meal)
}

func SearchMealByName(w http.ResponseWriter, r *http.Request) {
	meals, err := service.SearchMealByName(getValue(r))
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, meals)
}

func SearchMealByCategory(w http.ResponseWriter, r *http.Request) {
	meals, err := service.SearchMealBy(getValue(r), model.CATEGORY)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, meals)
}

func SearchMealByArea(w http.ResponseWriter, r *http.Request) {
	meals, err := service.SearchMealBy(getValue(r), model.AREA)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, meals)
}

func SearchMealByIngredient(w http.ResponseWriter, r *http.Request) {
	meals, err := service.SearchMealBy(getValue(r), model.INGREDIENT)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, meals)
}

func GetCategoryOverview(w http.ResponseWriter, r *http.Request) {
	categoryOverview, err := service.GetFilter(model.CATEGORY)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, categoryOverview)
}

func GetAreaOverview(w http.ResponseWriter, r *http.Request) {
	areaOverview, err := service.GetFilter(model.AREA)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, areaOverview)
}

func GetIngredientOverview(w http.ResponseWriter, r *http.Request) {
	ingredientOverview, err := service.GetFilter(model.INGREDIENT)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, ingredientOverview)
}
