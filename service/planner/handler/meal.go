package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/service"
)

func getUserMeal(r *http.Request) (*model.UserMeal, error) {
	var meal model.UserMeal
	err := json.NewDecoder(r.Body).Decode(&meal)
	if err != nil {
		return nil, err
	}

	return &meal, nil
}

func getGroupMeal(r *http.Request) (*model.GroupMeal, error) {
	var meal model.GroupMeal
	err := json.NewDecoder(r.Body).Decode(&meal)
	if err != nil {
		return nil, err
	}

	return &meal, nil
}

func CreateUserMeal(w http.ResponseWriter, r *http.Request) {
	meal, err := getUserMeal(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := service.CreateUserMeal(meal); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, meal)
}

func DeleteUserMeal(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = service.DeleteUserMeal(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, nil)
}

func CreateGroupMeal(w http.ResponseWriter, r *http.Request) {
	meal, err := getGroupMeal(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := service.CreateGroupMeal(meal); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, meal)
}

func DeleteGroupMeal(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = service.DeleteGroupMeal(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, nil)
}

func CreateFavoriteMeal(w http.ResponseWriter, r *http.Request) {
	userId, err := getUserId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	mealId, err := getMealId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := service.CreateFavoriteMeal(userId, mealId); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, nil)
}

func DeleteFavoriteMeal(w http.ResponseWriter, r *http.Request) {
	userId, err := getUserId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	mealId, err := getMealId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = service.DeleteFavoriteMeal(userId, mealId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, nil)
}
