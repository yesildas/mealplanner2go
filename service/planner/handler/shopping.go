package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/service"
)

func getShoppingDate(r *http.Request) (*model.ShoppingDate, error) {
	var shoppingDate model.ShoppingDate
	err := json.NewDecoder(r.Body).Decode(&shoppingDate)
	if err != nil {
		return nil, err
	}

	return &shoppingDate, nil
}

func CreateShoppingDate(w http.ResponseWriter, r *http.Request) {
	shoppingDate, err := getShoppingDate(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	if err := service.CreateShoppingDate(shoppingDate); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, shoppingDate)
}

func DeleteShoppingDate(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = service.DeleteShoppingDate(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	sendJson(w, nil)
}

func GetShoppingList(w http.ResponseWriter, r *http.Request) {
	id, err := getId(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	users, err := service.GetShoppingList(id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	sendJson(w, users)
}
