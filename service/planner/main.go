package main

import (
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/db"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/handler"
)

func init() {
	// init db
	err := db.Init()
	if err != nil {
		log.Fatal(err)
	}

	// init logger
	log.SetFormatter(&log.TextFormatter{})
	log.SetReportCaller(true)
	level, err := log.ParseLevel(os.Getenv("LOG_LEVEL"))
	if err != nil {
		log.Info("Log level not specified, set default to: INFO")
		log.SetLevel(log.InfoLevel)
		return
	}
	log.SetLevel(level)
}

func main() {
	port := 8000
	router := mux.NewRouter()
	router.HandleFunc("/user/list/{id}", handler.GetShoppingList).Methods(http.MethodGet)
	router.HandleFunc("/user/date/{id}", handler.DeleteShoppingDate).Methods(http.MethodDelete)
	router.HandleFunc("/user/date", handler.CreateShoppingDate).Methods(http.MethodPost)
	router.HandleFunc("/user/meal/{id}", handler.DeleteUserMeal).Methods(http.MethodDelete)
	router.HandleFunc("/user/meal", handler.CreateUserMeal).Methods(http.MethodPost)
	router.HandleFunc("/user/{userId}/{mealId}", handler.CreateFavoriteMeal).Methods(http.MethodPost)
	router.HandleFunc("/user/{userId}/{mealId}", handler.DeleteFavoriteMeal).Methods(http.MethodDelete)
	router.HandleFunc("/user/{id}", handler.GetUser).Methods(http.MethodGet)
	router.HandleFunc("/user/{id}", handler.UpdateUser).Methods(http.MethodPut)
	router.HandleFunc("/user/{id}", handler.DeleteUser).Methods(http.MethodDelete)
	router.HandleFunc("/user", handler.GetUsers).Methods(http.MethodGet)
	router.HandleFunc("/user", handler.CreateUser).Methods(http.MethodPost)
	router.HandleFunc("/group/meal/{id}", handler.DeleteGroupMeal).Methods(http.MethodDelete)
	router.HandleFunc("/group/meal", handler.CreateGroupMeal).Methods(http.MethodPost)
	router.HandleFunc("/group/{groupId}/{userId}", handler.CreateUserGroupRelation).Methods(http.MethodPost)
	router.HandleFunc("/group/{groupId}/{userId}", handler.DeleteUserGroupRelation).Methods(http.MethodDelete)
	router.HandleFunc("/group/{id}", handler.GetGroup).Methods(http.MethodGet)
	router.HandleFunc("/group/{id}", handler.DeleteGroup).Methods(http.MethodDelete)
	router.HandleFunc("/group", handler.GetGroups).Methods(http.MethodGet)
	router.HandleFunc("/group", handler.CreateGroup).Methods(http.MethodPost)
	router.HandleFunc("/meal/random", handler.GetRandomMeal).Methods(http.MethodGet)
	router.HandleFunc("/meal/search/{value}", handler.SearchMealByName).Methods(http.MethodGet)
	router.HandleFunc("/meal/category/{value}", handler.SearchMealByCategory).Methods(http.MethodGet)
	router.HandleFunc("/meal/category", handler.GetCategoryOverview).Methods(http.MethodGet)
	router.HandleFunc("/meal/area/{value}", handler.SearchMealByArea).Methods(http.MethodGet)
	router.HandleFunc("/meal/area", handler.GetAreaOverview).Methods(http.MethodGet)
	router.HandleFunc("/meal/ingredient/{value}", handler.SearchMealByIngredient).Methods(http.MethodGet)
	router.HandleFunc("/meal/ingredient", handler.GetIngredientOverview).Methods(http.MethodGet)
	router.HandleFunc("/meal/{id}", handler.GetMeal).Methods(http.MethodGet)
	log.Infof("Server is listening on port :%v", port)
	if err := http.ListenAndServe(fmt.Sprintf(":%v", port), router); err != nil {
		log.Fatal(err)
	}
}
