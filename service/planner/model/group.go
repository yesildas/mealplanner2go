package model

type Group struct {
	ID      int               `json:"id"`
	Name    string            `json:"name"`
	UserIDs []int             `json:"userIds"`
	Meals   []GroupMealWithId `json:"meals"`
}

type GroupItem struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
