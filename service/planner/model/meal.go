package model

type UserMeal struct {
	UserID int    `json:"userId"`
	MealID int    `json:"mealId"`
	Date   string `json:"date"`
}

type UserMealWithId struct {
	ID     int    `json:"id"`
	UserID int    `json:"userId"`
	MealID int    `json:"mealId"`
	Date   string `json:"date"`
}

type GroupMeal struct {
	GroupID int    `json:"groupId"`
	MealID  int    `json:"mealId"`
	Date    string `json:"date"`
}

type GroupMealWithId struct {
	ID      int    `json:"id"`
	GroupID int    `json:"groupId"`
	MealID  int    `json:"mealId"`
	Date    string `json:"date"`
}

type Ingredient struct {
	Name   string `json:"name"`
	Amount string `json:"amount"`
}

type Meal struct {
	ID           int           `json:"id"`
	Name         string        `json:"name"`
	Category     string        `json:"category"`
	Ingredients  []*Ingredient `json:"ingredients"`
	Instructions string        `json:"instructions"`
	ImagePath    string        `json:"imagePath"`
}

type MealOverview struct {
	ID        int    `json:"id"`
	Name      string `json:"name"`
	ImagePath string `json:"imagePath"`
}

type FilterOverview struct {
	ValidValues []string `json:"validValues"`
}

const (
	CATEGORY   Filter = "c"
	AREA       Filter = "a"
	INGREDIENT Filter = "i"
)

type Filter string
