package model

type ShoppingDate struct {
	UserID int    `json:"userId"`
	Date   string `json:"date"`
}

type ShoppingDateWithId struct {
	ID     int    `json:"id"`
	UserID int    `json:"userId"`
	Date   string `json:"date"`
}

type ShoppingLists struct {
	Name  string                   `json:"name"`
	Lists map[string][]*Ingredient `json:"lists"`
}
