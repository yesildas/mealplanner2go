package model

type User struct {
	ID            int                  `json:"id"`
	Mail          string               `json:"mail"`
	FirstName     string               `json:"firstName"`
	LastName      string               `json:"lastName"`
	GroupIds      []int                `json:"groupIds"`
	FavoriteMeal  []int                `json:"favoriteMeals"`
	Meals         []UserMealWithId     `json:"meals"`
	ShoppingDates []ShoppingDateWithId `json:"shoppingDates"`
}

type UserItem struct {
	ID        int    `json:"id"`
	Mail      string `json:"mail"`
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
}
