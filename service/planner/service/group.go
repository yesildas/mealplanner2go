package service

import (
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/db"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
)

func CreateGroup(group *model.Group) (*model.Group, error) {
	group, err := db.CreateGroup(group.Name)
	if err != nil {
		return nil, err
	}
	log.Infof("Group created with ID: %d", group.ID)

	return group, nil
}

func GetGroup(id int) (*model.Group, error) {
	group, err := db.GetGroup(id)
	if err != nil {
		return nil, err
	}
	log.Infof("Group retrieved with ID: %d", group.ID)

	return group, nil
}

func GetGroups() ([]*model.GroupItem, error) {
	groups, err := db.GetGroups()
	if err != nil {
		return nil, err
	}
	log.Infof("Groups retrieved")

	return groups, nil
}

func DeleteGroup(id int) (*model.Group, error) {
	group, err := db.DeleteGroup(id)
	if err != nil {
		return nil, err
	}
	log.Infof("Group deleted with ID: %d", group.ID)

	return group, nil
}

func CreateUserGroupRelation(userId int, groupId int) error {
	err := db.CreateUserGroupRelation(userId, groupId)
	if err != nil {
		return err
	}
	log.Infof("User added to Group with IDs: %d to %d", userId, groupId)

	return nil
}

func DeleteUserGroupRelation(userId int, groupId int) error {
	err := db.DeleteUserGroupRelation(userId, groupId)
	if err != nil {
		return err
	}
	log.Infof("User removed from Group with IDs: %d from %d", userId, groupId)

	return nil
}
