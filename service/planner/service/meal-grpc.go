package service

import (
	"context"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/client"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/client/meal"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
	"google.golang.org/protobuf/types/known/emptypb"
)

func convertMealResponseToModel(res *meal.MealResponse) *model.Meal {
	result := &model.Meal{
		ID:           int(res.Id),
		Name:         res.Name,
		Category:     res.Category,
		Instructions: res.Instructions,
		ImagePath:    res.ImagePath,
	}

	for _, v := range res.Ingredients {
		ingredient := &model.Ingredient{
			Name:   v.Name,
			Amount: v.Amount,
		}
		result.Ingredients = append(result.Ingredients, ingredient)
	}

	return result
}

func convertMealResponsesToModel(res *meal.MealResponses) []*model.Meal {
	var result []*model.Meal
	for _, v := range res.Meals {
		result = append(result, convertMealResponseToModel(v))
	}

	return result
}

func convertMealOverviewResponseToModel(res *meal.MealOverviewResponse) *model.MealOverview {
	return &model.MealOverview{
		ID:        int(res.Id),
		Name:      res.Name,
		ImagePath: res.ImagePath,
	}
}

func convertMealOverviewResponsesToModel(res *meal.MealOverviewResponses) []*model.MealOverview {
	var result []*model.MealOverview
	for _, v := range res.Meals {
		result = append(result, convertMealOverviewResponseToModel(v))
	}

	return result
}

func GetRandomMeal() (*model.Meal, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	conn, err := client.GetMealConnection(ctx)
	if err != nil {
		log.Fatal("error connecting to the banktransfer service\n")
	}
	defer conn.Close()
	client := meal.NewMealClient(conn)
	res, err := client.GetRandomMeal(ctx, &emptypb.Empty{})
	if err != nil {
		return nil, err
	}
	log.Infof("Random meal requested")

	return convertMealResponseToModel(res), nil
}

func GetMeal(id int) (*model.Meal, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	conn, err := client.GetMealConnection(ctx)
	if err != nil {
		log.Fatal("error connecting to the banktransfer service\n")
	}
	defer conn.Close()
	client := meal.NewMealClient(conn)
	res, err := client.GetMeal(ctx, &meal.MealRequest{Id: int32(id)})
	if err != nil {
		return nil, err
	}
	log.Infof("Meal requested with ID: %d", id)

	return convertMealResponseToModel(res), nil
}

func SearchMealByName(name string) ([]*model.Meal, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	conn, err := client.GetMealConnection(ctx)
	if err != nil {
		log.Fatal("error connecting to the banktransfer service\n")
	}
	defer conn.Close()
	client := meal.NewMealClient(conn)
	res, err := client.SearchMealByName(ctx, &meal.SearchRequest{Name: name})
	if err != nil {
		return nil, err
	}
	log.Infof("Meal searched with: %s", name)

	return convertMealResponsesToModel(res), nil
}

func SearchMealBy(value string, filter model.Filter) ([]*model.MealOverview, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	conn, err := client.GetMealConnection(ctx)
	if err != nil {
		log.Fatal("error connecting to the banktransfer service\n")
	}
	defer conn.Close()
	client := meal.NewMealClient(conn)
	res, err := client.SearchMealBy(ctx, &meal.MealOverviewRequest{Value: value, Filter: string(filter)})
	if err != nil {
		return nil, err
	}
	log.Infof("Meal categorized / areazied / ingredientized with: %s", value)

	return convertMealOverviewResponsesToModel(res), nil
}

func GetFilter(filter model.Filter) (*model.FilterOverview, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	conn, err := client.GetMealConnection(ctx)
	if err != nil {
		log.Fatal("error connecting to the banktransfer service\n")
	}
	defer conn.Close()
	client := meal.NewMealClient(conn)
	res, err := client.GetFilter(ctx, &meal.FilterOverviewRequest{Filter: string(filter)})
	if err != nil {
		return nil, err
	}
	log.Infof("Meal filter requested (Category, Area, Ingredient)")

	return &model.FilterOverview{
		ValidValues: res.ValidValues,
	}, nil
}
