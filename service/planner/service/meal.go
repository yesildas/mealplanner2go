package service

import (
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/db"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
)

func CreateUserMeal(meal *model.UserMeal) error {
	err := db.CreateUserMeal(meal.UserID, meal.MealID, meal.Date)
	if err != nil {
		return err
	}
	log.Infof("User meal added for User: %d", meal.UserID)

	return nil
}

func DeleteUserMeal(id int) error {
	err := db.DeleteUserMeal(id)
	if err != nil {
		return err
	}
	log.Infof("User meal deleted")

	return nil
}

func CreateGroupMeal(meal *model.GroupMeal) error {
	err := db.CreateGroupMeal(meal.GroupID, meal.MealID, meal.Date)
	if err != nil {
		return err
	}
	log.Infof("Group meal added for Group: %d", meal.GroupID)

	return nil
}

func DeleteGroupMeal(id int) error {
	err := db.DeleteGroupMeal(id)
	if err != nil {
		return err
	}
	log.Infof("Group meal deleted")

	return nil
}

func CreateFavoriteMeal(userId int, mealId int) error {
	err := db.CreateFavoriteMeal(userId, mealId)
	if err != nil {
		return err
	}
	log.Infof("Favorite meal added for User: %d", userId)

	return nil
}

func DeleteFavoriteMeal(userId int, mealId int) error {
	err := db.DeleteFavoriteMeal(userId, mealId)
	if err != nil {
		return err
	}
	log.Infof("Favorite meal deleted")

	return nil
}
