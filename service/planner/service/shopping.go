package service

import (
	"errors"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/db"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
)

func CreateShoppingDate(shoppingDate *model.ShoppingDate) error {
	err := db.CreateShoppingDate(shoppingDate.UserID, shoppingDate.Date)
	if err != nil {
		return err
	}
	log.Infof("Shopping date added for User: %d", shoppingDate.UserID)

	return nil
}

func DeleteShoppingDate(id int) error {
	err := db.DeleteShoppingDate(id)
	if err != nil {
		return err
	}
	log.Infof("Shopping date deleted with ID: %d", id)

	return nil
}

func GetShoppingList(id int) ([]*model.ShoppingLists, error) {
	var result []*model.ShoppingLists
	log.Infof("Shopping list request for User: %d", id)
	user, err := db.GetUser(id)
	if err != nil {
		return nil, err
	}
	var dates []time.Time
	for _, v := range user.ShoppingDates {
		date, err := time.Parse(time.RFC3339, v.Date)
		if err != nil {
			return nil, err
		}
		timeNow := time.Now().Truncate(24 * time.Hour)
		if timeNow.Equal(date) || timeNow.Before(date) {
			dates = append(dates, date)
		}
	}

	log.Infof("Possible shopping dates retrieved: %d", len(dates))
	if len(dates) == 0 {
		return nil, errors.New("possible shopping dates are missing")
	}

	var userList model.ShoppingLists
	userList.Name = user.FirstName
	userList.Lists = make(map[string][]*model.Ingredient)
	if len(user.Meals) != 0 {
		for _, v := range user.Meals {
			date, err := time.Parse(time.RFC3339, v.Date)
			if err != nil {
				return nil, err
			}
			currentIdx := 0
			lowestDuration := 255 * time.Hour * 24 * 365
			for idx, d := range dates {
				if date.Equal(d) || date.After(d) {
					if date.Sub(d) < lowestDuration {
						currentIdx = idx
						lowestDuration = date.Sub(d)
					}
				}
			}
			if lowestDuration != 255*time.Hour*24*365 {
				meal, err := GetMeal(v.MealID)
				if err != nil {
					return nil, err
				}

				key := dates[currentIdx].Format(time.DateOnly)
				userList.Lists[key] = append(userList.Lists[key], meal.Ingredients...)
			}
		}
	}
	result = append(result, &userList)
	for _, groupId := range user.GroupIds {
		group, err := GetGroup(groupId)
		if err != nil {
			continue
		}
		var groupList model.ShoppingLists
		groupList.Name = group.Name
		groupList.Lists = make(map[string][]*model.Ingredient)
		groupDates := make(map[int][]time.Time)
		for _, userId := range group.UserIDs {
			groupUser, err := GetUser(userId)
			if err != nil {
				continue
			}
			for _, v := range groupUser.ShoppingDates {
				date, err := time.Parse(time.RFC3339, v.Date)
				if err != nil {
					return nil, err
				}
				timeNow := time.Now().Truncate(24 * time.Hour)
				if timeNow.Equal(date) || timeNow.Before(date) {
					groupDates[userId] = append(groupDates[userId], date)
				}
			}
		}

		log.Infof("Possible shopping dates for %d group members retrieved", len(groupDates))
		if len(group.Meals) != 0 {
			for _, v := range group.Meals {
				date, err := time.Parse(time.RFC3339, v.Date)
				if err != nil {
					return nil, err
				}
				currentUserId := 0
				currentIdx := 0
				lowestDuration := 255 * time.Hour * 24 * 365
				for id, dates := range groupDates {
					for idx, d := range dates {
						if date.Equal(d) || date.After(d) {
							if date.Sub(d) < lowestDuration {
								currentUserId = id
								currentIdx = idx
								lowestDuration = date.Sub(d)
							}
						}
					}
				}
				log.Infof("Check if user has to add group meal to the shopping list")
				if lowestDuration != 255*time.Hour*24*365 {
					if currentUserId == id {
						meal, err := GetMeal(v.MealID)
						if err != nil {
							return nil, err
						}
						key := groupDates[currentUserId][currentIdx].Format(time.DateOnly)
						groupList.Lists[key] = append(groupList.Lists[key], meal.Ingredients...)
					}
				}
			}
		}
		result = append(result, &groupList)
	}
	log.Infof("Shopping list request is Done for User: %d", id)

	return result, nil
}
