package service

import (
	log "github.com/sirupsen/logrus"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/db"
	"gitlab.reutlingen-university.de/yesildas/mealplanner2go/service/planner/model"
)

func CreateUser(user *model.User) (*model.User, error) {
	user, err := db.CreateUser(user.Mail, user.FirstName, user.LastName)
	if err != nil {
		return nil, err
	}
	log.Infof("User created with ID: %d", user.ID)

	return user, nil
}

func GetUser(id int) (*model.User, error) {
	user, err := db.GetUser(id)
	if err != nil {
		return nil, err
	}
	log.Infof("User retrieved with ID: %d", user.ID)

	return user, nil
}

func GetUsers() ([]*model.UserItem, error) {
	users, err := db.GetUsers()
	if err != nil {
		return nil, err
	}
	log.Infof("Users retrieved")

	return users, nil
}

func UpdateUser(id int, user *model.User) (*model.User, error) {
	user, err := db.UpdateUser(id, user.Mail, user.FirstName, user.LastName)
	if err != nil {
		return nil, err
	}
	log.Infof("User updated with ID: %d", user.ID)

	return user, nil
}

func DeleteUser(id int) (*model.User, error) {
	user, err := db.DeleteUser(id)
	if err != nil {
		return nil, err
	}
	log.Infof("User deleted with ID: %d", user.ID)

	return user, nil
}
